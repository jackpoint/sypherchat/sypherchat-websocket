import os
import json
import boto3

from boto3.dynamodb.conditions import Key, Attr

CONNECTIONS_TABLE = os.getenv("CONNECTIONS_TABLE", "sypher_connections")
ENDPOINT_URL = None

if os.getenv("AWS_SAM_LOCAL"):
    ENDPOINT_URL = "http://dynamodb:8000"


def _cleanup_old_connections(connections_table, connection_id, username):
    response = connections_table.query(
        IndexName="UsernameIndex",
        KeyConditionExpression=Key("username").eq(username),
    )
    connections = response["Items"]
    for connection in connections:
        if connection["connectionId"] != connection_id:
            connections_table.delete_item(
                Key={
                    "connectionId": connection["connectionId"],
                }
            )


def lambda_handler(event, context):
    connection_id = event["requestContext"]["connectionId"]
    username = event["queryStringParameters"]["username"]
    connection = {"connectionId": connection_id, "username": username}
    dynamodb = boto3.resource("dynamodb", endpoint_url=ENDPOINT_URL)
    connections_table = dynamodb.Table(CONNECTIONS_TABLE)
    connections_table.put_item(Item=connection)

    # Cleanup any old connections
    _cleanup_old_connections(connections_table, connection_id, username)

    return {
        "statusCode": 200,
        "body": json.dumps(
            {
                "connectionId": connection_id,
                "username": username,
            }
        ),
    }
