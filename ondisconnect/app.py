import os
from sqlite3 import connect
import boto3

CONNECTIONS_TABLE = os.getenv("CONNECTIONS_TABLE", "sypher_connections")
ENDPOINT_URL = None

if os.getenv("AWS_SAM_LOCAL"):
    ENDPOINT_URL = "http://dynamodb:8000"


def lambda_handler(event, context):
    print("event:::")
    print(event)
    connection_id = event["requestContext"]["connectionId"]
    dynamodb = boto3.resource("dynamodb", endpoint_url=ENDPOINT_URL)
    connections_table = dynamodb.Table(CONNECTIONS_TABLE)
    response = connections_table.delete_item(Key={"connectionId": connection_id})

    return {"statusCode": 200}
