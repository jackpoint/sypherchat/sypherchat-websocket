import os
import json
import string
import random

from unittest import mock
from uuid import uuid4

import pytest
import boto3
from moto import mock_dynamodb2
from faker import Faker

fake = Faker(seed=0)


class MockContext(object):
    def __init__(self, function_name):
        self.function_name = function_name
        self.function_version = "v$LATEST"
        self.memory_limit_in_mb = 512
        self.invoked_function_arn = (
            f"arn:aws:lambda:us-east-1:ACCOUNT:function:{self.function_name}"
        )
        self.aws_request_id = str(uuid4)


@pytest.fixture(scope="function")
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"


@pytest.fixture
def lambda_context():
    return MockContext("dummy_function")


@pytest.fixture()
def connection_id():
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=16))


@pytest.fixture()
def username():
    return fake.user_name()


@pytest.fixture()
def token():
    return fake.password(length=100, special_chars=False, upper_case=False)


@pytest.fixture()
def apigw_event(connection_id, username, token):
    """Generates API GW Event"""
    with open("./events/connect_event.json", "r") as fp:
        data = json.load(fp)

        data["requestContext"]["connectionId"] = connection_id
        data["multiValueQueryStringParameters"]["username"] = username
        data["multiValueQueryStringParameters"]["token"] = token
        data["queryStringParameters"]["username"] = username
        data["queryStringParameters"]["token"] = token

        return data


@pytest.fixture()
def apigw_disconnect_event(connection_id):
    with open("./events/disconnect_event.json", "r") as fp:
        data = json.load(fp)

        data["requestContext"]["connectionId"] = connection_id

        return data


@pytest.fixture()
def apigw_sendmessage_event(connection_id):
    with open("./events/sendmessage_event.json", "r") as fp:
        data = json.load(fp)

        data["requestContext"]["connectionId"] = connection_id

        return data


@pytest.fixture()
def apigw_send_response(connection_id):
    with open("./events/send_response.json", "r") as fp:
        data = json.load(fp)
        return data


@pytest.fixture(scope="function")
def mock_connections(aws_credentials):
    with mock_dynamodb2():
        dynamodb = boto3.resource("dynamodb")
        dynamodb.create_table(
            AttributeDefinitions=[
                {"AttributeName": "connectionId", "AttributeType": "S"},
                {"AttributeName": "username", "AttributeType": "S"},
            ],
            TableName="sypher_connections",
            KeySchema=[
                {"AttributeName": "connectionId", "KeyType": "HASH"},
            ],
            GlobalSecondaryIndexes=[
                {
                    "IndexName": "UsernameIndex",
                    "KeySchema": [
                        {"AttributeName": "username", "KeyType": "HASH"},
                    ],
                    "Projection": {
                        "ProjectionType": "ALL",
                    },
                },
            ],
        )
        yield dynamodb.Table("sypher_connections")


@pytest.fixture(scope="function")
def mock_existing_connection(mock_connections, apigw_event):
    with mock_dynamodb2():
        connection_id = apigw_event["requestContext"]["connectionId"]
        username = apigw_event["queryStringParameters"]["username"]
        connection = {"connectionId": connection_id, "username": username}
        mock_connections.put_item(Item=connection)
        yield mock_connections


@pytest.fixture(scope="function")
def mock_messages(aws_credentials):
    with mock_dynamodb2():
        dynamodb = boto3.resource("dynamodb")
        dynamodb.create_table(
            AttributeDefinitions=[
                {"AttributeName": "room", "AttributeType": "S"},
                {"AttributeName": "index", "AttributeType": "N"},
            ],
            TableName="messages",
            KeySchema=[
                {"AttributeName": "room", "KeyType": "HASH"},
                {"AttributeName": "index", "KeyType": "RANGE"},
            ],
        )
        yield dynamodb.Table("messages")
