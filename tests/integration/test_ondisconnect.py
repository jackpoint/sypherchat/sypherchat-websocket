from ondisconnect import app


def test_lambda_handler_success(
    apigw_disconnect_event, mock_connections, username, connection_id, lambda_context
):
    connection = {"connectionId": connection_id, "username": username}
    mock_connections.put_item(Item=connection)

    assert mock_connections.scan()["Items"] == [connection]

    mock_connections.delete_item(Key={"connectionId": connection_id})
    app.lambda_handler(apigw_disconnect_event, lambda_context)

    assert mock_connections.scan()["Items"] == []
