import pytest
import json

import botocore.exceptions

from datetime import datetime
from unittest.mock import patch, Mock

from sendmessage import app

EXPECTED_DATE = datetime(2021, 10, 1).isoformat()


@pytest.mark.freeze_time(EXPECTED_DATE)
@patch("sendmessage.app.boto3.client")
def test_lambda_handler_success(
    mock_client,
    apigw_send_response,
    apigw_sendmessage_event,
    mock_messages,
    mock_connections,
    username,
    connection_id,
    lambda_context,
):
    instance = mock_client.return_value
    instance.post_to_connection.return_value = apigw_send_response

    connection = {"connectionId": connection_id, "username": username}
    mock_connections.put_item(Item=connection)

    assert app.lambda_handler(apigw_sendmessage_event, lambda_context) == {
        "statusCode": 200
    }

    expected_resource = "apigatewaymanagementapi"
    expected_domain = apigw_sendmessage_event["requestContext"]["domainName"]
    expected_stage = apigw_sendmessage_event["requestContext"]["stage"]
    mock_client.assert_called_with(
        expected_resource,
        endpoint_url=f"https://{expected_domain}/{expected_stage}",
    )

    data = json.loads(apigw_sendmessage_event["body"])["data"]
    expected_data = {"username": username, "data": data, "createdAt": EXPECTED_DATE}

    instance.post_to_connection.assert_called_with(
        ConnectionId=connection_id, Data=json.dumps(expected_data).encode("utf-8")
    )


@patch("sendmessage.app.boto3.client")
def test_lambda_handler_client_error(
    mock_client,
    apigw_send_response,
    apigw_sendmessage_event,
    mock_messages,
    mock_connections,
    username,
    connection_id,
    lambda_context,
):
    def side_effect(ConnectionId, Data):
        if ConnectionId == connection_id:
            return apigw_send_response

        return botocore.exceptions.ClientError(
            {"Error": {"Code": "GoneException", "Message": "This Failed"}},
            "PostToConnection",
        )

    instance = mock_client.return_value
    instance.post_to_connection = Mock(side_effect=side_effect)

    connection = {"connectionId": connection_id, "username": username}
    mock_connections.put_item(Item=connection)
    disappeared_connection = {"connectionId": "disappeared", "username": "nope"}
    mock_connections.put_item(Item=disappeared_connection)

    assert app.lambda_handler(apigw_sendmessage_event, lambda_context) == {
        "statusCode": 200
    }

    expected_resource = "apigatewaymanagementapi"
    expected_domain = apigw_sendmessage_event["requestContext"]["domainName"]
    expected_stage = apigw_sendmessage_event["requestContext"]["stage"]
    mock_client.assert_called_with(
        expected_resource,
        endpoint_url=f"https://{expected_domain}/{expected_stage}",
    )
