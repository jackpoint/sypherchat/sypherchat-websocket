import pytest
import json

from onconnect import app


def test_lambda_handler_success(mock_connections, apigw_event, lambda_context):
    assert mock_connections.scan()["Items"] == []
    expected_connection_id = apigw_event["requestContext"]["connectionId"]
    expected_username = apigw_event["queryStringParameters"]["username"]
    ret = app.lambda_handler(apigw_event, lambda_context)

    assert ret["statusCode"] == 200
    
    assert ret["body"] == json.dumps({
        "connectionId": expected_connection_id,
        "username": expected_username,
    })


def test_lambda_handler_cleanup_old_connections(
    mock_connections,
    apigw_event,
    lambda_context,
    username,
    connection_id,
):
    old_connection = {"connectionId": "anOldConnection", "username": username}
    mock_connections.put_item(Item=old_connection)
    expected_connection = {"connectionId": connection_id, "username": username}

    app.lambda_handler(apigw_event, lambda_context)
    assert mock_connections.scan()["Items"] == [expected_connection]
