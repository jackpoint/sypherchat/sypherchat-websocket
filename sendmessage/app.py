import os

import boto3
import ast
import json
from datetime import datetime
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
import logging

logger = logging.getLogger("dev")
logger.setLevel(logging.DEBUG)
client = boto3.client("apigatewaymanagementapi")

CONNECTIONS_TABLE = os.getenv("CONNECTIONS_TABLE", "sypher_connections")
ENDPOINT_URL = None

if os.getenv("AWS_SAM_LOCAL"):
    ENDPOINT_URL = "http://dynamodb:8000"


def _get_endpoint_url(event):
    domain_name = event["requestContext"]["domainName"]
    stage = event["requestContext"]["stage"]

    return f"https://{domain_name}/{stage}"


def _send_to_connection(connection_id, data, event):
    gatewayapi = boto3.client(
        "apigatewaymanagementapi", endpoint_url=_get_endpoint_url(event)
    )
    response = gatewayapi.post_to_connection(
        ConnectionId=connection_id, Data=json.dumps(data).encode("utf-8")
    )
    print(response)
    # print("response>>>>>>>")
    return response


def _get_connection_id(event):
    return event["requestContext"]["connectionId"]


def _get_current_connection(connections_table, connection_id):
    response = connections_table.get_item(Key={"connectionId": connection_id})
    return response["Item"]


def _format_data(event):
    message_body = event["body"]
    message2 = ast.literal_eval(message_body)
    print("message2", message2)
    return message2["data"]


def lambda_handler(event, context):
    print("???????????What's up nerds????")
    print("!!!!!!!!!!!!!SEND_MESSAGE!!!!!!!!!!!!!")
    print(event)

    # Get existing connections
    dynamodb = boto3.resource("dynamodb", endpoint_url=ENDPOINT_URL)
    connections_table = dynamodb.Table(CONNECTIONS_TABLE)

    # Get current connection
    connection_id = _get_connection_id(event)
    # Find existing connection
    current_connection = _get_current_connection(connections_table, connection_id)

    username = current_connection["username"]
    data = _format_data(event)

    # Broadcast to all connections
    print(data)
    logger.debug("Broadcasting message: {}".format(data))
    current_timestamp = datetime.now()
    data = {"username": username, "data": data, "createdAt": current_timestamp.isoformat()}
    valresp = ""

    response = connections_table.scan()
    connections = response["Items"]

    # print("connections:::")
    print(connections)

    for connection in connections:
        try:
            valresp = _send_to_connection(connection["connectionId"], data, event)
        except ClientError as e:
            print(f"Failed to send message: {e}")
            continue

    print(valresp)
    return {"statusCode": 200}
